function getDate(date,european){
    var date=new Date(date);
    var day=date.getDate()
    day=(day>9)?day:'0'+day;
    var month=date.getMonth()+1
    month=(month>9)?month:'0'+month;
    var hours=date.getHours();
    hours=(hours>9)?hours:'0'+hours;
    var minutes=date.getMinutes();
    minutes=(minutes>9)?minutes:'0'+minutes;
    var seconds=date.getSeconds();
    seconds=(seconds>9)?seconds:'0'+seconds;
    if (european){
        return (day)+'/'+(month)+'/'+date.getFullYear()+' '+hours+':'+minutes;
    }
    else{
        return date.getFullYear()+'-'+(month)+'-'+(day)+' '+hours+':'+minutes+':'+seconds;
    }
    
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}