function getCurrentUser(){
    return remote.getGlobal('user');
}

function getDB(name){
    return remote.getGlobal('db')[name];
}

function getIMG(name){
    return 'file://'+remote.getGlobal('utils').rootPath+'/images/'+name;
}