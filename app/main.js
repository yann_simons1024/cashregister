const {app, BrowserWindow, ipcMain} = require('electron')
const path = require('path')
const url = require('url')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

global.user={
    id : '',
    name : ''
};

global.utils={
  rootPath : app.getPath('userData')+'/appResources/'
}

//Create the DB
var Datastore = require('nedb');
global.db={
    articles : new Datastore({filename: global.utils.rootPath+'/db/articles.db', autoload: true}),
    articlesType : new Datastore({filename: global.utils.rootPath+'/db/articlesType.db', autoload: true}),
    bills : new Datastore({filename: global.utils.rootPath+'/db/bills.db', autoload: true, timestampData : true}),
    onwait : new Datastore({filename: global.utils.rootPath+'/db/onwait.db', autoload: true, timestampData : true}),
    members : new Datastore({filename: global.utils.rootPath+'/db/members.db', autoload: true})
}; 
global.db.articlesType.ensureIndex({fieldName: 'type', unique: true});
global.db.articles.ensureIndex({fieldName: 'name', unique: true});
global.db.members.ensureIndex({fieldName: 'name', unique: true});
global.db.bills.ensureIndex({fieldName: 'active'});
global.db.onwait.ensureIndex({fieldName: 'active'});

//DB backup
const fs = require('fs-extra')
try {
    var dateFile = new Date();
    var month=dateFile.getMonth()+1;
    month=(month<10)?'0'+month:month;
    var day=dateFile.getDate();
    day=(day<10)?'0'+day:day;
    var hours=dateFile.getHours();
    hours=(hours<10)?'0'+hours:hours;
    var minutes=dateFile.getMinutes();
    minutes=(minutes<10)?'0'+minutes:minutes;
    var seconds=dateFile.getSeconds();
    seconds=(seconds<10)?'0'+seconds:seconds;
    dateFile=dateFile.getFullYear()+'-'+month+'-'+day+'/'+hours+minutes+seconds;
    fs.copySync(global.utils.rootPath+'/db/articles.db', global.utils.rootPath+'/backup/'+dateFile+'-articles.db')
    fs.copySync(global.utils.rootPath+'/db/articlesType.db', global.utils.rootPath+'/backup/'+dateFile+'-articlesType.db')
    fs.copySync(global.utils.rootPath+'/db/members.db', global.utils.rootPath+'/backup/'+dateFile+'-members.db')
    fs.copySync(global.utils.rootPath+'/db/onwait.db', global.utils.rootPath+'/backup/'+dateFile+'-onwait.db')
    fs.copySync(global.utils.rootPath+'/db/bills.db', global.utils.rootPath+'/backup/'+dateFile+'-bills.db')
    console.log('Save DB : success!')
} 
catch (err) {
    console.error(err);
}

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({
      fullscreen : false,
      resizable : false,
      autoHideMenuBar : true,
      width : 500,
      height : 300
  });
  
  // and load the first page of the app.
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'login.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})