function update(database, id, record, callback){
    if (id==''){
        database.insert(record,function(err, doc){
            if (typeof callback == 'function'){
                callback(err, doc);
            }
        });
    }
    else{
        database.update({_id:id},record,{
            returnUpdatedDocs : true
        },function(err, numReplaced, affectedDocuments, upsert){
            if (typeof callback == 'function'){
                callback(err, affectedDocuments);
            }                        
        });
    }
}

function remove(database, id, callback){
    database.remove({_id:{$in:id}},{ multi: true },function(err,numRemoved){
        callback(err,numRemoved);
    });
}